package com.polytech.cloud.controller;

import com.polytech.cloud.exception.APIRequestException;
import com.polytech.cloud.exception.UserException;
import com.polytech.cloud.exception.UserNotFoundException;
import com.polytech.cloud.exception.UserValidationException;
import com.polytech.cloud.model.EntityUser;
import com.polytech.cloud.service.UserService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Date;
import java.util.List;

@Validated
@Api(tags = {"UserController"})
@RestController
@RequestMapping(
        value = "/user",
        produces = {MediaType.APPLICATION_JSON_VALUE})
public class UserController {

    private final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    private final UserService serviceUser;

    @Autowired
    public UserController(UserService serviceUser) {
        this.serviceUser = serviceUser;
    }

    @ApiOperation(value = "Récupère les utilisateurs (avec pagination)")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<EntityUser> getAllUsers(
            @ApiParam(value = "Numéro de la page à retirer")
            @RequestParam(value = "page", required = false, defaultValue = "0") int page) {
        return serviceUser.findAll(page).getContent();
    }

    @ApiOperation(value = "Supprime tous les utilisateurs de la base")
    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public void deleteAllUsers() {
        serviceUser.deleteAll();
    }

    @ApiOperation(value = "Remplace tous les utilisateurs de la base")
    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    public List<EntityUser> replaceAllUsers(
            @ApiParam(value = "Liste des nouveaux utilisateurs à insérer", required = true)
            @Valid @RequestBody List<EntityUser> users) {
        return serviceUser.replaceAll(users);
    }

    @ApiOperation(value = "Récupère un utilisateur selon son ID")
    @ApiResponses(value = {@ApiResponse(code = 404, message = "Not found")})
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public EntityUser getUserById(
            @ApiParam(value = "ID à rechercher", required = true)
            @PathVariable(value = "id") String id) throws UserNotFoundException {
        return serviceUser.findById(id);
    }

    @ApiOperation(value = "Récupère les utilisateurs (avec pagination) selon un nom")
    @GetMapping(value = "/search")
    @ResponseStatus(HttpStatus.OK)
    public List<EntityUser> getUserByLastName(
            @ApiParam(value = "Nom à rechercher", required = true)
            @RequestParam("term") String name,
            @ApiParam(value = "Numéro de la page")
            @RequestParam(value = "page", required = false, defaultValue = "0") int page) {
        return serviceUser.findByLastName(name, page).getContent();
    }

    @ApiOperation(value = "Récupère les utilisateurs (avec pagination) au dessus d'un âge")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Invalid age")})
    @GetMapping(value = "/age", params = "gt")
    @ResponseStatus(HttpStatus.OK)
    public List<EntityUser> getUsersByGreaterAge(
            @ApiParam(value = "Âge à rechercher", required = true)
            @RequestParam(value = "gt") int ageMin,
            @ApiParam(value = "Numéro de la page")
            @RequestParam(value = "page", required = false, defaultValue = "0") int page) throws APIRequestException {
        if (ageMin > 0) {
            return serviceUser.findOlderThan(ageMin, page).getContent();
        } else {
            throw new APIRequestException("Dis donc petit malin, depuis quand on a un âge négatif ? ;)");
        }
    }

    @ApiOperation(value = "Récupère les utilisateurs (avec pagination) selon leur âge exact")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Invalid age")})
    @GetMapping(value = "/age", params = "eq")
    @ResponseStatus(HttpStatus.OK)
    public List<EntityUser> getUsersByEqualAge(
            @ApiParam(value = "Âge à rechercher", required = true)
            @RequestParam(value = "eq") int ageExact,
            @ApiParam(value = "Numéro de la page")
            @RequestParam(value = "page", required = false, defaultValue = "0") int page) throws APIRequestException {
        if (ageExact > 0) {
            return serviceUser.findByExactAge(ageExact, page).getContent();
        } else {
            throw new APIRequestException("Dis donc petit malin, depuis quand on a un âge négatif ? ;)");
        }
    }

    @ApiOperation(value = "Récupère les utilisateurs les plus proches d'un point")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Invalid position")})
    @GetMapping(value = "/nearest")
    @ResponseStatus(HttpStatus.OK)
    public List<EntityUser> getUsersByPosition(
            @ApiParam(value = "Latitude du point", required = true)
            @Min(value = -90) @Max(value = 90) @RequestParam(value = "lat") Double lat,
            @ApiParam(value = "Longitude du point", required = true)
            @Min(value = -180) @Max(value = 180) @RequestParam(value = "lon") Double lon) {
        return serviceUser.findByPosition(lat, lon);
    }

    @ApiOperation(value = "Ajoute un utilisateur")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EntityUser insertUser(
            @ApiParam(value = "Détails de l'utilisateur", required = true)
            @Valid @RequestBody EntityUser user) {
        // On supprime l'ID potentiellement tapé par l'utilisateur (il sera auto-généré par MongoDB lors de l'ajout).
        user.setId(null);
        return serviceUser.insert(user);
    }

    @ApiOperation(value = "Met un jour un utilisateur")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Invalid user"),
            @ApiResponse(code = 404, message = "Not found")})
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public EntityUser updateUser(
            @ApiParam(value = "ID de l'utilisateur à modifier", required = true)
            @PathVariable("id") String id,
            @ApiParam(value = "Nouvelles valeurs des détails", required = true)
            @RequestBody EntityUser newUser) throws UserNotFoundException, UserValidationException {
        // Tous les champs de l'entité dans la requête remplaceront les anciennes valeurs de l'entité en base.
        EntityUser oldUser = serviceUser.findById(id);
        String newFirstName = newUser.getFirstName();
        String newLastName = newUser.getLastName();
        Date newBirthDay = newUser.getBirthDay();
        EntityUser.Position newPosition = newUser.getPosition();

        // Contrôle des informations.
        if (newFirstName != null && !newFirstName.isEmpty()) oldUser.setFirstName(newFirstName);
        if (newLastName != null && !newLastName.isEmpty()) oldUser.setLastName(newLastName);
        if (newBirthDay != null) {
            if (newBirthDay.before(new Date())) oldUser.setBirthDay(newBirthDay);
            else throw new UserValidationException("La date d'anniversaire ne peut pas être dans le turfu.");
        }
        if (newPosition != null) {
            if (newPosition.getLat() != null && newPosition.getLon() != null) oldUser.setPosition(newPosition); //TODO: A affiner (dans la bonne range).
            else throw new UserValidationException("La position de l'utilisateur ne peut pas être vide.");
        }

        return serviceUser.update(oldUser);
    }

    @ApiOperation(value = "Supprime un utilisateur selon l'ID")
    @ApiResponses(value = {@ApiResponse(code = 500, message = "Not found")})
    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteUserById(
            @ApiParam(value = "ID de l'utilisateur à supprimer", required = true)
            @PathVariable("id") String id) throws UserException {
        /* Encapsulation de l'erreur spécifique UserNotFoundException dans l'erreur générique UserException dû à une mauvaise conception
        des tests client. */
        try {
            serviceUser.findById(id); // Pour trigger si nécessaire l'erreur UserNotFoundException.
            serviceUser.delete(id);
        } catch (UserNotFoundException e) {
            throw new UserException(e);
        }
    }

}
