package com.polytech.cloud.service;

import com.polytech.cloud.exception.APIRequestException;
import com.polytech.cloud.exception.UserNotFoundException;
import com.polytech.cloud.model.EntityUser;
import com.polytech.cloud.repositories.RepositoryUser;
import com.polytech.cloud.utility.DateUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@CacheConfig(cacheNames = { "users" }) // Localisation des caches pour cette classe
public class UserService {

    private final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    private final RepositoryUser repositoryUser;

    @Value("${polytech.cloud.user.page-size}")
    private Integer PAGE_SIZE;

    @Autowired
    public UserService(RepositoryUser repositoryUser) {
        this.repositoryUser = repositoryUser;
    }

    /**
     * Retourne les utilisateurs pour une page donnée.
     * @param page Numéro de la page
     * @return Page contenant les entités
     * @throws APIRequestException Si le numéro de la page est négatif
     */
    @Cacheable(key = "#page") // Stockage selon la page
    public Page<EntityUser> findAll(int page) throws APIRequestException {
        if (page < 0) {
            throw new APIRequestException("L'index de la page doit être positif");
        }

        Pageable pageable = PageRequest.of(page, PAGE_SIZE);
        return repositoryUser.findAll(pageable);
    }

    /**
     * Récupère l'utilisateur selon son ID.
     * @param id ID à rechercher
     * @return Détails de l'utilisateur
     * @throws UserNotFoundException Si l'utilisateur n'existe pas
     */
    public EntityUser findById(String id) throws UserNotFoundException {
        return repositoryUser.findById(id)
                .orElseThrow(() -> new UserNotFoundException(String.format("Aucun utilisateur avec ID = '%s'", id)));
    }

    /**
     * Récupère les utilisateurs (avec pagination) selon leur nom.
     * @param lastName Nom à rechercher.
     * @param page Numéro de la page.
     * @return Page contenant les entités
     * @throws APIRequestException Si le numéro de la page est négatif
     */
    public Page<EntityUser> findByLastName(String lastName, int page) throws APIRequestException {
        if (page < 0) {
            throw new APIRequestException("L'index de la page doit être positif");
        }

        Pageable pageable = PageRequest.of(page, PAGE_SIZE);
        return repositoryUser.findByLastName(lastName, pageable);
    }

    /**
     * Récupère les utilisateurs (avec pagination) selon leur âge.
     * @param age Âge minimal des utilisateurs
     * @param page Numéro de la page
     * @return Page contenant les entités
     * @throws APIRequestException Si le numéro de la page est négatif
     */
    public Page<EntityUser> findOlderThan(int age, int page) throws APIRequestException {
        if (page < 0) {
            throw new APIRequestException("L'index de la page doit être positif");
        }

        Pageable pageable = PageRequest.of(page, PAGE_SIZE);
        LocalDate birthDate = DateUtility.getMinBirthDate(age);

        return repositoryUser.findByBirthDayBefore(DateUtility.localDateToDate(birthDate), pageable);
    }

    /**
     * Récupère les utilisateurs (avec pagination) selon leur âge.
     * @param age Âge exact des utilisateurs
     * @param page Numéro de la page
     * @return Page contenant les entités
     * @throws APIRequestException Si le numéro de la page est négatif
     */
    public Page<EntityUser> findByExactAge(int age, int page) throws APIRequestException {
        if (page < 0) {
            throw new APIRequestException("L'index de la page doit être positif");
        }

        Pageable pageable = PageRequest.of(page, PAGE_SIZE);
        LocalDate maxBirthDate = DateUtility.getMinBirthDate(age);
        LocalDate minBirthDate = DateUtility.getMinBirthDate(age + 1);

        return repositoryUser.findByBirthDayBetween(DateUtility.localDateToDate(minBirthDate), DateUtility.localDateToDate(maxBirthDate), pageable);
    }

    //TODO: A faire (ou pas)
    public List<EntityUser> findByPosition(Double longitude, Double latitude) {
        return new ArrayList<>();
    }

    /**
     * Ajoute un utilisateur en base.
     * @param user Utilisateur à ajouter.
     * @return Utilisateur ajouté (avec ID mis à jour)
     */
    public EntityUser insert(EntityUser user) {
        // L'ID est automatiquement généré lors de l'ajout.
        return repositoryUser.save(user);
    }

    /**
     * Ajoute tous les utilisateurs en base
     * @param users Utilisateurs à ajouter.
     * @return Utilisateur ajouté (avec ID mis à jour)
     */
    public List<EntityUser> addAll(List<EntityUser> users) {
        return repositoryUser.saveAll(users);
    }

    /**
     * Met à jour un utilisateur.
     * @param oldUser Utilisateur à mettre à jour
     * @return Utilisateur mis à jour
     */
    public EntityUser update(EntityUser oldUser) {
        return repositoryUser.save(oldUser);
    }

    /**
     * Supprime un utilisateur
     * @param id ID de l'utilisateur à supprimer.
     */
    public void delete(String id) {
        repositoryUser.deleteById(id);
    }

    /**
     * Supprime tous les utilisateurs en base.
     */
    @CacheEvict(allEntries = true) // Vide l'entiereté du cache.
    public void deleteAll() {
        repositoryUser.deleteAll();
    }

    /**
     * Remplace tous les utilisateurs en base (vide la base puis la remplit).
     * @param users Utilisateurs à ajouter
     * @return Utilisateurs ajoutés (avec ID mis à jour)
     */
    public List<EntityUser> replaceAll(List<EntityUser> users) {
        deleteAll();
        return addAll(users);
    }
}
