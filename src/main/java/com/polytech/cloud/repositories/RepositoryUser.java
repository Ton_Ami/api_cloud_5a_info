package com.polytech.cloud.repositories;

import com.mongodb.client.model.geojson.Point;
import com.polytech.cloud.model.EntityUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.geo.Distance;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface RepositoryUser extends MongoRepository<EntityUser, Integer> {

    /**
     * Récupère l'utilisateur dont l'id est en paramètre
     * @param id: String
     * @return Optional<EntityUser>
     */
    Optional<EntityUser> findById(String id);

    /**
     * Récupère les utilisateurs dont le nom est en paramètre
     * @param lastName: String
     * @param pageable: Pageable
     * @return Page<EntityUser>
     */
    Page<EntityUser> findByLastName(String lastName, Pageable pageable);

    /**
     * Récupère les 100 premiers utilisateurs nés avant la date donnée en paramètre
     * @param birthDate: Date
     * @param pageable: Pageable
     * @return Page<EntityUser>
     */
    Page<EntityUser> findByBirthDayBefore(Date birthDate, Pageable pageable);

    /**
     * Récupère les 100 premiers utilisateurs nés entre les deux dates en paramètres
     * @param date1: Date
     * @param date2: Date
     * @param pageable: Pageable
     * @return Page<EntityUser>
     */
    Page<EntityUser> findByBirthDayBetween(Date date1, Date date2, Pageable pageable);

    List<EntityUser> findByPositionNear(Point point, Distance distance);

    /**
     * Supprime l'utilisateur dont l'id est en paramètre
     * @param id: String
     */
    void deleteById(String id);

}
