package com.polytech.cloud.config;

import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;


@Configuration
@EnableSwagger2
@SwaggerDefinition(tags = {
        @Tag(name = "UserController", description = "API pour les opérations sur les utilisateurs.") // TODO: Description non affichée.
})
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfiguration {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.polytech.cloud.controller"))
                .paths(PathSelectors.any())
                .build()
                .useDefaultResponseMessages(false) // Supprime l'affichage des codes HTTP par défaut
                .apiInfo(apiEndPointsInfo());
    }

    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder()
                .title("Documentation User API")
                .description("User Management REST API")
                .contact(new Contact("Équipe 6", "www.useless-site.com", "useless-email@gmail.com"))
//                .termsOfServiceUrl("")
                .license("Apache 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
                .version("1.0.0")
                .build();
    }
}