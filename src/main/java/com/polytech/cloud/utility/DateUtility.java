package com.polytech.cloud.utility;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * Classe utilisée pour les calculs liés à l'âge et aux dates de naissance
 */
public class DateUtility {

    /**
     * Récupère la date de naissance minimale en fonction de l'âge en paramètre
     * @param age: int
     * @return LocalDate
     */
    public static LocalDate getMinBirthDate(int age) {
        // récupère la date d'aujourd'hui
        LocalDate today = LocalDate.now();
        int year = today.getYear();
        int month = today.getMonthValue();
        int day = today.getDayOfMonth();

        int birthYear = year - age;

        return LocalDate.of(birthYear, month, day);
    }

    /**
     * Convertit un objet LocalDate en objet Date
     * @param localDate: LocalDate
     * @return Date
     */
    public static Date localDateToDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
}
