package com.polytech.cloud.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;
import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.Date;

@ApiModel(description = "Tous les détails à propos de l'utilisateur.")
@Document(collection = "users")
public class EntityUser {

    @ApiModelProperty(notes = "La BD génère automatiquement l'ID", required = true)
    private String id;

    @ApiModelProperty(notes = "Date d'anniversaire", required = true, position = 1)
    private Date birthDay;

    @ApiModelProperty(notes = "Prénom", required = true, position = 2)
    private String firstName;

    @ApiModelProperty(notes = "Nom", required = true, position = 3)
    private String lastName;

    @ApiModelProperty(notes = "Position 2D", required = true, position = 4)
    private Position position;

    // Static donc n'a pas accès aux informations de la classe encapsulante (EntityUser).
    @ApiModel(description = "Tous les détails sur les coordonnées d'un point d'une sphère.")
    public static class Position {

        @ApiModelProperty(notes = "Latitude", required = true, position = 1)
        private Double lat;

        @ApiModelProperty(notes = "Longitude", required = true, position = 2)
        private Double lon;

        @Field(value = "lat")
        @NotNull(message = "Latitude is mandatory")
        @Min(value = -90, message = "Latitude has to be greater than -90.")
        @Max(value = 90, message = "Latitude has to be less than 90.")
        public Double getLat() {
            return lat;
        }

        public void setLat(Double lat) {
            this.lat = lat;
        }

        @Field(value = "lon")
        @NotNull(message = "Longitude is mandatory")
        @Min(value = -180, message = "Longitude has to be greater than -180.")
        @Max(value = 180, message = "Longitude has to be less than 180.")
        public Double getLon() {
            return lon;
        }

        public void setLon(Double lon) {
            this.lon = lon;
        }
    }

    @Id
    public String getId() {
        return id;
    }

    public void setId(String id) { this.id = id; }

    @Field(value = "birthDay")
//    @NotEmpty(message = "Birthday is mandatory") //TODO: Vérification de (date non vide) ne marche pas.
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    @JsonFormat(pattern = "MM/dd/yyyy")
    @Past
    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    @Field(value = "firstName")
    @NotEmpty(message = "First name is mandatory")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Field(value = "lastName")
    @NotEmpty(message = "Last name is mandatory")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Valid
    @Field(value = "position")
    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EntityUser that = (EntityUser) o;

        if (!id.equals(that.id)) return false;
        if (!birthDay.equals(that.birthDay)) return false;
        if (!firstName.equals(that.firstName)) return false;
        if (!lastName.equals(that.lastName)) return false;
        return position.equals(that.position);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + birthDay.hashCode();
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + position.hashCode();
        return result;
    }
}

